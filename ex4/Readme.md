# Exercise 4: Secure your container

In this exercise, you will:
- execute your app as specific user
- set filesystem to read-only
- try to hack your container

## Vulnerable nodejs app for demos

**WARNING**: *This app deliberately exposes a RCE vulnerability (CVE-2013-4660). It is meant to demonstrate the use of Docker to clean up after a breach and prevent them from happening again in the future.*

#### Step 1: Change user
The first task is to get the image running as a user instead of root. To do this you’ll need to edit the `Dockerfile`​.
First you have to add a new group `groupadd -r myuser`. Having done so, add a user to this group: `useradd -r -g myuser myuser`.
Within the `Dockerfile` you can switch the user, that is used to execute the commands, by adding `USER myuser`.

    vi Dockerfile
    docker build -t node-hack .
    docker rm -f node-hack
    docker run -d --rm -p 1337:1337 --name node-hack node-hack

Check that the application still works as before.

#### Step 2: Read-Only filesystem

Modify the ​docker run​ statement to start the container with a read-only filesystem (use the image you created in step 1):

    docker rm -f node-hack
    docker run -d --read-only --rm -p 1337:1337 --name node-hack node-hack
  
### Step 3: Hack again

Run the attacks from exercise 2 again. Note any differences from the previous results.
