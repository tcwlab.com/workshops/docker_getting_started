# Exercise 3: Isolate and analyse

In this exercise, you will:
- run & hack your container
- isolate your container
- analyse your container

## Vulnerable nodejs app for demos

**WARNING**: *This app deliberately exposes a RCE vulnerability (CVE-2013-4660). It is meant to demonstrate the use of Docker to clean up after a breach and prevent them from happening again in the future.*

#### Step 1: Run & hack
If it isn’t running already, relaunch the application and execute ​`yaml/evil.yml`​:

    docker rm -f node-hack
    docker run -d -p 1337:1337 --name node-hack node-hack

#### Step 2: Isolate container

The first thing we want to do is disconnect the application from the network. To do this, run:

    docker network disconnect bridge node-hack

If you now try to load the application in the browser, it should be inaccessible.

### Step 3: Analyse container

Optional: Take a look at the different `docker` commands for getting information on the running container, which you can find in `​docker --help​` and at <http://docs.docker.com>. In particular, try `inspect`​, `​top` ​and​ `logs`.

If you’re concerned that the container isn’t sufficiently isolated, or want to stop rogue processes that are running in the container, you can do so with ​`docker stop​`. Run this now:

    docker stop node-hack

This has stopped the container and all processes running inside it, but left the filesystem intact. Therefore we can still run:

    docker diff node-hack

This will show any differences in the filesystem inside the container, compared to the image it was built from. In our case, we can see a new ​`index.html` ​file has been created, corresponding to our hacked frontpage. If a hacker had installed any tools or scripts, we would see those too.

