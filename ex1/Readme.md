# Exercise 1: Hello World

In this exercise, you will:
- build a image from a Dockerfile
- create a container from your own image

## Content

In this folder you will find a file called `message.txt`.
Goal is, that the content of this file will be printed to stdout
on container execution.

What you have to do:
- create a Startscript
- fill the Dockerfile
- create the image
- run the container

